import pandas as pd

job_table = pd.read_csv("jobs_dur_nn 1.csv")
job_table = job_table[['duration','num_nodes']]
job_table = job_table.dropna()

df_filtered = job_table[job_table['duration'] != 0]

baseline_proba = {
4:0.9904791327394925,
6: 0.989734,
12: 0.987541,
24: 0.983344,
32: 0.980618,
64: 0.970192,
96: 0.960492
}

p00 = 0.98

def select_FW(job_time):
    FW = [4,6,12,24,32,64,96]
    required_timesteps = job_time/15

    for fw in FW:
        if(required_timesteps<=fw):
            selected_FW = fw
            break
    return selected_FW

def get_FW_idx(fw):
    FW = [4,6,12,24,32,64,96]
    for idx in range(len(FW)):
        if(fw == FW[i]):
            return idx

def calculate_confusion_matrix_probabilities(p_pred, p_true):
    """
    Calculate the probabilities of True Positive, False Positive, False Negative, and True Negative.
 
    Parameters:
    p_pred (float): The probability of predicting class 1.
    p_true (float): The actual proportion of class 1 in the data.
 
    Returns:
    dict: A dictionary containing the probabilities of TP, FP, FN, TN.
    """
    TP = p_pred * p_true
    FP = p_pred * (1 - p_true)
    FN = (1 - p_pred) * p_true
    TN = (1 - p_pred) * (1 - p_true)
 
    return {"TP": TP, "FP": FP, "FN": FN, "TN": TN}

def combined_classifier_probabilities(N, p_TP, p_FP, p_TN, p_FN):
    """
    N: Number of classifiers.
    p_TP: True positive probability of a single classifier.
    p_TN: True negative probability of a single classifier.
    p_FP: False positive probability of a single classifier.
    p_FN: False negative probability of a single classifier.
    """
    p_TP_combined = p_TP ** N
    p_TN_combined = 1 - (p_FP ** N)
    p_FP_combined = p_FP ** N
    p_FN_combined = 1 - (p_TP ** N)
 
    return {
        'True Positive Probability': p_TP_combined,
        'True Negative Probability': p_TN_combined,
        'False Positive Probability': p_FP_combined,
        'False Negative Probability': p_FN_combined
    }

combined_fw_dict = {}
for i in [4,6,12,24,32,64,96]:
  combined_fw_dict[i] = calculate_confusion_matrix_probabilities(pow((p00),i), baseline_proba[i])

def wrapper(N,T,fw_dict):
    FW = select_FW(T)
    p_TP = fw_dict[FW]['TP']
    p_FP = fw_dict[FW]['FP']
    p_TN = fw_dict[FW]['TN']
    p_FN = fw_dict[FW]['FN']
    #p_TP, p_FP, p_TN, p_FN = fw_dict[FW]
    return combined_classifier_probabilities(N, p_TP, p_FP, p_TN, p_FN)

duration = df_filtered['duration'].to_numpy()
num_nodes = df_filtered['num_nodes'].to_numpy()

job_baseline_prob = []
for idx in range(len(duration)):
    FW_idx = get_FW_idx(select_FW(duration[idx]))
    job_baseline_prob.append(wrapper(num_nodes[idx],duration[idx],combined_fw_dict[FW_idx]))

tp = 0
tn = 0
fp = 0
fn = 0

jobs_length = len(job_baseline_prob)

for prob in job_baseline_prob:
    tp = tp + prob['True Positive Probability']
    tn = tn + prob['True Negative Probability']
    fp = fp + prob['False Positive Probability']
    fn = fn + prob['False Negative Probability']

print(f"TP: {tp/jobs_length}, TN: {tn/jobs_length}, FP: {fp/jobs_length}, FN: {fn/jobs_length}")  