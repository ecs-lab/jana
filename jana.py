import os
import torch
import numpy as np
from torch_geometric.data import Data
from torch_geometric.loader import DataLoader
from torch_geometric.nn import GCNConv
import time

def time_to_minutes(time_str):
    # Split the time string into hours, minutes, and seconds
    hours, minutes, seconds = map(int, time_str.split(':'))
    # Calculate the total minutes
    total_minutes = hours * 60 + minutes
    return total_minutes

def get_spatial_distribution():
    spatial_distribution = []

    for rack in range(49):
        for i in range(20):
            node = (rack*20)+i
            spatial_distribution.append((rack,node))
    return spatial_distribution

def get_node_pos(spatial_distribution,rack,node):
    count = 0
    for pos in spatial_distribution:
        if(pos[0]==rack):
            count = count + 1
            if(count == node):
                return count

def select_FW(job_time):
    FW = [4,6,12,24,32,64,96]
    required_timesteps = (time_to_minutes(job_time))/15

    for fw in FW:
        if(required_timesteps<=fw):
            selected_FW = fw
            break
    return selected_FW

def select_racks(nodes_list,spatial_distribution):
    racks_selected = []

    for node in nodes_list:
        for node_pos in spatial_distribution:
            if(node == node_pos[1]):
                racks_selected.append(node_pos[0])
    return racks_selected

def select_GNN_model_for_job(job_time,nodes_list,models_dir,device):
    spatial_distribution = get_spatial_distribution()
    selected_fw = select_FW(job_time)
    selected_racks = select_racks(nodes_list,spatial_distribution)

    print("\nSelected FW: {}\nSelected racks: {}".format(selected_fw,selected_racks))

    models = []
    for rack in selected_racks:
        PATH = os.path.join(models_dir,"{}/{}_{}.pth".format(selected_fw,rack,selected_fw))

        model = anomaly_anticipation(417,16)
        if device == 'cuda':
            model.load_state_dict(torch.load(PATH))
        else:
            model.load_state_dict(torch.load(PATH, map_location=torch.device('cpu')))
        models.append(model)

    return models

nodes_in_rack = [(0, 16), (1, 17), (2, 17), (3, 17), (4, 16), (5, 11), (6, 15), \
 (7, 15), (8, 19), (9, 19), (10, 13), (11, 18), (12, 18), (13, 19), \
(14, 19), (15, 17), (16, 17), (17, 17), (18, 18), (19, 18), (20, 17), (21, 17), \
(22, 14), (23, 18), (24, 17), (25, 20), (26, 17), (27, 18), (28, 19), (29, 17), \
(30, 18), (31, 16), (32, 20), (33, 15), (34, 20), (35, 20), (36, 18), (37, 15), \
(38, 19), (39, 18), (40, 17), (41, 20), (42, 18), (43, 17), (44, 20), (45, 20), \
(46, 20), (47, 18), (48, 18)]

def make_edge_index(num_nodes):
    edges = []
    for i in range(num_nodes):
        temp = []
        if i == 0:
            temp.append([i,i+1])
        elif i == num_nodes-1:
            temp.append([i,i-1])
        else:
            temp.append([i,i-1])
            temp.append([i,i+1])
        edges = edges + temp

    edges = torch.tensor(edges, dtype=torch.long)

    return edges

def inference(models,graphs):
    node_prediction = []
    for i in range(len(models)):
        model = models[i]
        graph = graphs[i]
        out = model(graph.x,graph.edge_index)
        pred = torch.sigmoid(out)
        node_prediction.append(pred)
    return node_prediction

class anomaly_anticipation(torch.nn.Module):
    def __init__(self, in_channels, out_channels):
        super().__init__()
        #encoder
        self.conv1 = GCNConv(in_channels, 300)
        self.conv2 = GCNConv(300, 100)
        self.conv3 = GCNConv(100, out_channels)

        #dense layer
        self.fc1 = torch.nn.Linear(out_channels,16)
        self.fc2 = torch.nn.Linear(16,1)

    def forward(self, x, edge_index):
        x = self.conv1(x, edge_index).relu()
        x = self.conv2(x, edge_index).relu()
        x = self.conv3(x, edge_index).relu()
        x = self.fc1(x)
        x = self.fc2(x)
        return x


models_dir = "model"
#setting up cuda
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

job_time  = "01:00:00" # set the job time
nodes_list = [0,50,975] # arbitrary nodes selected for th job

print("JOB:")
print("Time: {} \nNodes list: {}".format(job_time,nodes_list))

# run for random graphs generation
def random_graph(nodes_in_rack,rack):
    # Set the seed for reproducibility
    np.random.seed(42)
    num_nodes = nodes_in_rack[rack][1]
    edges = make_edge_index(num_nodes)

    data_list = []
    for i in range(num_nodes):
        # Create a random feature tensor with 417 elements
        feature_tensor = torch.tensor(np.random.rand(417),dtype=torch.float)
        data_list.append(feature_tensor)

    data_list = torch.stack(data_list)
    rack_graphs = Data(x=data_list, edge_index=edges.t().contiguous())
    return rack_graphs

selected_racks = select_racks(nodes_list,get_spatial_distribution())
spatial_distribution=get_spatial_distribution()

#print("Selected racks: {}".format(selected_racks))

graphs = []

for rack in selected_racks:
    graphs.append(random_graph(nodes_in_rack,rack))

start_time = time.perf_counter()
rack_inference = inference(select_GNN_model_for_job(job_time,nodes_list,models_dir,device),graphs)
end_time = time.perf_counter()

elapsed_time = end_time - start_time
print("Inference time per node: {}".format(elapsed_time/len(nodes_list)))
job_node_availability_prob = []

for i in range(len(nodes_list)):
    rack = selected_racks[i]
    node = nodes_list[i]
    pos = get_node_pos(spatial_distribution,rack,node)
    rack_inference[i][pos]
    job_node_availability_prob.append(1-(rack_inference[i][0][pos].item()))

print("\nJob node availabilty prob:")
print(job_node_availability_prob)

best_threshold = {"4":0.015170,"6":0.014256,"12":0.011668,"24":0.012105,"32":0.017430,\
                  "64":0.028987,"96":0.032550}

selected_threshold = best_threshold[str(select_FW(job_time))]

print("Selected threshold: {}".format(selected_threshold))

classify_node = []
for prob in job_node_availability_prob:
    if prob<=selected_threshold:
        classify_node.append(0)
    else:
        classify_node.append(1)

print("\nClassified nodes:")
print(classify_node)